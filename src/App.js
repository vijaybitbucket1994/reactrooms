import React, { Component } from 'react';

import Hotels  from "./components/Hotels";

class App extends Component {
  render() {
    return (
      <div className="App container">
        <div className="row">
          <div className="col-xs-12 mx-auto">
          <div className="header">
          <i className="fa fa-group fa-2x px-2"></i> 
          <h3 className="d-flex"> Choose number of people
          </h3>
          </div>
          <Hotels/>
           </div>
        </div>  
      </div>

    );
  }
}

export default App;
