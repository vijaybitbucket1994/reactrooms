import React, {Component} from "react";


class Count extends Component{

    constructor(props){
        super(props); 
        this.handleIncrement=this.handleIncrement.bind(this);
        this.handleDecrement=this.handleDecrement.bind(this);
        this.state={
            incre:false,
            decre:false
        }
    }
    

    handleIncrement(e){
        this.setState({decre:false});
         const dataobj={
            name:e.target.name,
            count:Number(e.target.value)
        }
        // if(dataobj.name == "rooms" && dataobj.count >=4){
        //     this.setState({incre:true});
        // }

    
        // else{
        //     this.setState({incre:false});
        // }
        

        this.props.onIncrement(dataobj);
    }

    handleDecrement(e){
        this.setState({incre:false});
        const dataobj={
            name:e.target.name,
            count:Number(e.target.value)
            
        }
        // if((dataobj.name == "rooms" && dataobj.count <=2)||(dataobj.name == "adults" && dataobj.count <=2) ){
        //     this.setState({decre:true});
        // }
        
     
        // else if(dataobj.name == "children" && dataobj.count <=1){
        //     this.setState({decre:true});
        // }

        // else{
        //     this.setState({decre:false});
        // }
        
  
      
        this.props.onDecrement(dataobj);

    }


    render(){

        const name=this.props.name;
        var dataIncre=`${name}incre`;
        return(
          
            <div className="btn-container">
                 <i className={this.props.icon}></i>
                <label>{this.props.name}
                </label>

                <button type="button" onClick={this.handleIncrement} className="btn" name={this.props.name} value={this.props.count} id="btn-incre" disabled={this.state.incre} data-name={dataIncre}> + </button>
                     {this.props.count}
                 <button type="button" onClick={this.handleDecrement} className="btn" name={this.props.name} value={this.props.count} id="btn-decre"  disabled={this.state.decre} data-name={this.props.name}>-</button>
             </div>
        )
    }
}

export default Count;