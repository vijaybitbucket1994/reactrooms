import React, {Component} from "react";
import Count from "./Count";

class Hotels extends Component {

    constructor(props) {
        super(props);
        this.state = {
            count: {
                rooms: 1,
                adults: 1,
                children: 0
            }
        };
        this.increment = this.increment.bind(this);
        this.decrement = this.decrement.bind(this);
    }

    increment(dataobj) {
        var roomscount = this.state.count["rooms"];
        var adultscount = this.state.count["adults"];
        var childrencount = this.state.count["children"];

        var cvalue = this.state.count[dataobj.name];
        if (dataobj.name == "rooms") {
         
            if (this.state.count[dataobj.name] < 5) {
                document.querySelector('[data-name="adultsincre"]').disabled=false;
                document.querySelector('[data-name="childrenincre"]').disabled=false;
                document.querySelector('[data-name="rooms"]').disabled=false;
                this.setState(prevState => ({
                    count: {
                        ...prevState.count,
                        rooms: prevState.count.rooms + 1,
                        adults: prevState.count.adults+1
                    }
                }));
                document.querySelector('[data-name="adults"]').disabled= false;

                // var roomscount = this.state.count.rooms;
                // var adultscount = this.state.count.adults ;
                // referrence
                roomscount = this.state.count["rooms"];
                adultscount = this.state.count["adults"];
                   
            }
            else{
                document.querySelector('[data-name="roomsincre"]').disabled= true;
            }

        }
        
        else if (dataobj.name == "adults") {
            roomscount = this.state.count.rooms;
            adultscount = this.state.count.adults ;
            var totalcount=adultscount + childrencount;
            if((roomscount*4)>totalcount){
                this.setState(prevState => ({
                    count: {
                        ...prevState.count,
                        adults: prevState.count.adults+1
                    }
                }));
                adultscount = this.state.count["adults"];
                document.querySelector('[data-name="adults"]').disabled=false;
            }
            else{ 
                document.querySelector('[data-name="adultsincre"]').disabled=true;
            }

        } else {
            totalcount=adultscount + childrencount;
            if((roomscount*4)>totalcount){
                this.setState(prevState => ({
                    count: {
                        ...prevState.count,
                        children: prevState.count.children+1
                    }
                }));
                childrencount = this.state.count["children"];
                document.querySelector('[data-name="children"]').disabled=false;
            }
            else{
                document.querySelector('[data-name="childrenincre"]').disabled=true;
            }
        }
       

    }

    decrement(dataobj) {
    
        var roomscount = this.state.count["rooms"];
        var adultscount = this.state.count["adults"];
        var childrencount = this.state.count["children"];
        var cvalue = this.state.count[dataobj.name];
        if (dataobj.name == "rooms") {

            if (this.state.count[dataobj.name ] > 1) {

                this.setState(prevState => ({
                    count: {
                        ...prevState.count,
                        rooms: prevState.count.rooms-1
                    }
                }));
                document.querySelector('[data-name="adults"]').disabled=false;
                document.querySelector('[data-name="roomsincre"]').disabled=false;

                roomscount = this.state.count["rooms"] -1;
                let rooms_capacity = (roomscount) * 4;
                var tempadutscount = adultscount;
                var tempchildrencount = childrencount;

                let totalcount = tempadutscount + tempchildrencount;
                let verdict = rooms_capacity / totalcount;
                let remain=rooms_capacity % totalcount;

                // calculating constraints
                while ((verdict < 1) && (verdict >0)&& (remain !== 0) ){
                    console.log("initialverdict", verdict);
                    if (tempchildrencount == 0) {
                        // console.log("checking level2")
                        tempadutscount = --tempadutscount;
                        console.log("tempadutscount", tempadutscount);
                    
                    } else {

                        // console.log("checking");
                        tempchildrencount = --tempchildrencount;
                        console.log("tempchildrencount", tempchildrencount);
                    }
                    
                    totalcount = tempadutscount + tempchildrencount;
                    verdict = rooms_capacity  / totalcount;

                }

                this.setState(prevState => ({
                    count: {
                        ...prevState.count,
                        children: tempchildrencount,
                        adults: tempadutscount
                    }
                }));
                adultscount = this.state.count["adults"];
                console.log("adultscount",adultscount);
                childrencount = this.state.count["children"];
                console.log("childrencount",childrencount);

            }
            else{
                document.querySelector('[data-name="rooms"]').disabled=true;
            }
        } else if (dataobj.name == "adults") {
            
        //  limiting increment count depend on rooms
            if (this.state.count[dataobj.name] > 1 && (this.state.count["adults"] > this.state.count["rooms"]) ) {
                this.setState(prevState => ({
                    count: {
                        ...prevState.count,
                        adults: prevState.count.adults-1
                    }
                }));
                adultscount = this.state.count["adults"];
                document.querySelector('[data-name="adultsincre"]').disabled=false;
                document.querySelector('[data-name="childrenincre"]').disabled=false;
            }
            else{
                document.querySelector('[data-name="adults"]').disabled=true;
            }
        } else {
                    //  limiting increment count depend on rooms
            if (this.state.count[dataobj.name] > 0) {
                this.setState(prevState => ({
                    count: {
                        ...prevState.count,
                        children: prevState.count.children-1
                    }
                }));
                childrencount = this.state.count["children"];
                document.querySelector('[data-name="adultsincre"]').disabled=false;
                document.querySelector('[data-name="childrenincre"]').disabled=false;
            }
            else{
                document.querySelector('[data-name="children"]').disabled=true;
            }
            
        }

    
    }

    render() {
        var roomscount = this.state.count.rooms ;
        var adultscount = this.state.count.adults ;
        var childrencount = this.state.count.children ;
         console.log("roomscount",roomscount);
         console.log("adultscount",adultscount);
         console.log("childrencount",childrencount);
        //  document.querySelector('[data-name="adults"]').disabled=true;
        //  document.querySelector('[data-name="children"]').disabled=true;
        //  document.querySelector('[data-name="rooms"]').disabled=true;
        const rooms = "rooms";
        const adults = "adults";
        const children = "children";
        return (
            <div className="app-container row">
            <div className="col-xs-12">
                <Count
                    name="rooms"
                    count={roomscount}
                    onIncrement={this.increment}
                    onDecrement={this.decrement}
                    icon="fa fa-bed fa-2x" />
                    <hr className="divider"/>
                <Count
                    name="adults"
                    count={adultscount}
                    onIncrement={this.increment}
                    onDecrement={this.decrement}
                    icon="fa fa-male fa-2x"/>
                    <hr className="divider"/>
                <Count
                    name="children"
                    count={childrencount}
                    onIncrement={this.increment}
                    onDecrement={this.decrement}
                    icon="fa fa-child fa-2x"/>
                </div>
            </div>
        )
    }
}

export default Hotels;